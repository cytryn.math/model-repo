# Morpheus Model Repository

The [Morpheus model repository](https://morpheus.gitlab.io/models/) is an open-access data resource to store, search and retrieve unpublished and published computational models of spatio-temporal and multicellular biological systems, encoded in the MorpheusML language and readily executable with the [Morpheus software](http://morpheus.gitlab.io/download/latest/).

Want to contribute? Have a look at our [step-by-step guide](https://morpheus.gitlab.io/faq/models/submit/)!

## Documentation

### Minimum Model Requirements

A model in the repo consists at least of:

- a **MorpheusML file** (XML file),
- which is placed in a subcategory (e.g. `Contributed Exmaples` → `CPM` or `Published Models` → `Zebrafish` ) **inside its own folder** with a title that is used for the category-wise left-hand model navigation and should therefore be as short and precise as possible.

#### Optional Model Attachments

Furthermore, additional files associated with the model can be added to the model folder as desired, e.g.

- a model metadata and description supplementary file, named `index.md` (see below under ‘[Model Metadata and Description](#model-metadata-and-description)’),
- illustrations, diagrams, model graph etc.,
- videos,
- other XML model files etc.

### MorpheusML (XML)

Each entry in the Morpheus model repository comes with **at least one model file with the file extension `.xml`**. The sub-elements of the `<Description>`, `<Title>` and `<Details>`, are automatically migrated from the XML file to the model repo as (long) title and description respectively.

If more than one XML file should be added to a repo entry (in which, for example, features are switched on or off for demonstration purposes, different parameters are used, etc.), the **main model file must be flagged with the file name `model.xml`**. All complementary XML files can be given arbitrary file names.

If required, e.g. for more detailed model explanations, illustrations, videos, references etc., an optional model metadata and description file (see below under ‘[Model Metadata and Description](#model-metadata-and-description)’) can be added, which can also be used to overwrite the automatically generated title and description.

### Model Metadata and Description

A model entry can be **extended with further metadata and detailed descriptions** using the special model metadata and description file `index.md`. **We highly recommend making use of this possibility** to enrich the model with detailed descriptions, illustrations, sources, cross-references to other models, input data, own Morpheus implementation tips, teaching material, quizzes and so on!

#### Structure of `index.md`

The model metadata and description file `index.md` is subdivided into two main sections:

1. A so-called **front matter part with meta information** in the simple to understand YAML format,
2. a **content section** in which you can write your own texts in the easy-to-learn [Markdown](https://daringfireball.net/projects/markdown/syntax) syntax and embed your own uploaded or external media, LaTeX code, sources, etc.

To get an impression of the structure and the uncomplicated YAML/Markdown syntax, we recommend taking a look at existing model entries, such as [this one](https://gitlab.com/morpheus.lab/model-repo/-/blob/master/Published%20Models/Principles/Morphodynamics%201/index.md) (click on the `</>` button (‘Display source’) next to the file name `index.md` to display the unrendered code).

#### Morpheus Model ID

Each model is given a unique, unchangeable identifier. This can either be freely chosen and specified by the submitters in the `index.md` file or is selected automatically during the review process from the pool of available model IDs.

The model ID is assigned in the metadata part via the YAML frontmatter variable `MorpheusModelID` (see example below).

Unique identifiers follow the [regex](https://en.wikipedia.org/wiki/Regular_expression) pattern: **`^M[0-9]{4,}$`**, which means that the ID always consists of a sequence beginning with the letter ‘M’ (capitalised) followed by a four-digit number (padded with one or more zeros for numbers smaller than 1000).

Example:

- ID: `M0001`
- YAML-Code in  `index.md`: `MorpheusModelID: M0001`
- Resulting citable link: https://identifiers.org/morpheus/M0001

Each model published in the repository becomes automatically accessible via a persistent link with the scheme
`https://identifiers.org/morpheus/M....`, which can be cited, for example, in the 
code availability statement of your future papers.

More details about the Morpheus namespace on Identifiers.org can be found [here](https://registry.identifiers.org/registry/morpheus).

#### Publication Reference

In addition to the free text in the content-part of the `index.md`, where any content including references to the model can be added, there is the ability to have a **standardised reference section** generated via the metadata-part of the `index.md`.

An example of such a reference definition in the YAML format is shown below. The keyword `publication` is followed by the indented details of the publication such as the [DOI](https://www.doi.org) or the title:

```yaml
publication:
  doi: "10.1371/journal.pbio.3000708"
  # authors: ... (not necessary in category ‘Published Models’)
  title: "Reoccurring neural stem cell divisions in the adult zebrafish telencephalon are sufficient for the emergence of aggregated spatiotemporal patterns"
  journal: "PLoS Biology"
  volume: 18
  issue: 12
  page: "e3000708"
  year: 2020
  original_model: true
  ```

  This YAML code results in [this reference](https://morpheus.gitlab.io/model/m2984/#reference).

  There are a few **rules** to be considered:

  - **Minimum required parameters**: At least one author and the title must be given. If this minimum requirement is not met, the input is ignored and no reference section is generated.
    - In the category ‘[Published Models](https://morpheus.gitlab.io/model/published-models/)’, the **author(s) of the original publication must be mentioned with the global `authors` parameter**. These are also included in the reference section so that a separate `authors` parameter does not have to be specified again under `publication`.
    - Nevertheless, it is possible to specify a reference independently of the model author(s) in the category ‘[Contributed Examples](https://morpheus.gitlab.io/model/contributed-examples/)’, i.e. to **provide different author(s)** using the `authors` parameter under `publication`.
  - **Original or reproduced model**: In the category ‘Published Models’ there is the special parameter `original_model`. Set to the value `true`, it indicates that the model provided is the original one used in the publication. The value `false` means that the model used only provides a result reproduced with Morpheus that was originally obtained in the publication with a different simulator. According to the indication (`true`, `false` or none), the model is provided with a text note in the repo. Outside the category ‘Published Models’, the parameter `original_model` is ignored.
