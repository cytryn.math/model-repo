---
MorpheusModelID: M2061

authors: [E. G. Rens, L. Edelstein-Keshet]
contributors: [E. G. Rens]

title: "Cell Polarity and Migration: Generic ECM Dynamics"
date: "2021-09-02T11:33:00+02:00"
lastmod: "2021-12-19T15:10:00+01:00"

# Reference details
publication:
  doi: "10.1088/1478-3975/ac2888"
  title: "Cellular Tango: how extracellular matrix adhesion choreographs Rac-Rho signaling and cell movement"
  journal: "Phys. Biol."
  volume: 18
  issue: 6
  page: "066005"
  year: 2021
  original_model: true

tags:
- Adhesion
- Cell Deformation
- Cell-ECM Adhesion
- Cell Shape
- Cellular Potts Model
- CPM
- ECM
- Extracellular Matrix
- Front Protrusion
- GTPase
- Partial Differential Equation
- PDE
- Persistent Polarity
- Polarity Oscillation
- Protrusion-Retraction
- Rac
- Reaction–Diffusion System
- Rear Retraction
- Rho
- Spatial Pattern
- Spiral Wave

categories:
- DOI:10.1088/1478-3975/ac2888

# order
#weight: 10
---
<!-- Introduction -->
<!-- Description -->
<!-- Results -->
