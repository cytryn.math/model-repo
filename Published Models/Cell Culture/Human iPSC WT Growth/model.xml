<?xml version='1.0'?>
<MorpheusModel version="4">
    <Description>
        <Title>Libby2019</Title>
        <Details>Full title:Human iPSC wildtype colony growth.
Date: 	31.05.2021
Authors: 	A. R.G. Libby, D. Briers, I. Haghighi, D. A. Joy, B. R. Conklin, C. Belta, T. C. McDevitt
Curators: 	Lutz Brusch (lutz.brusch@tu-dresden.de)
Software: 	Morpheus (open-source), download from https://morpheus.gitlab.io
ModelID: 	https://identifiers.org/morpheus/M7671
Reference:This model is described in the peer-reviewed publication 
	A. R.G. Libby, D. Briers, I. Haghighi, D. A. Joy, B. R. Conklin, C. Belta, T. C. McDevitt.
          Automated Design of Pluripotent Stem Cell Self-Organization.
          Cell Systems 9(5): 483–495, 2019.
</Details>
    </Description>
    <Global>
        <Variable value="-100" symbol="ct1_adhesion"/>
        <Variable value="-100" symbol="ct2_adhesion"/>
        <Variable value="max(ct1_adhesion,ct2_adhesion)" symbol="ct1_ct2"/>
        <Constant value="0" symbol="ctall_medium_adhesion_init"/>
        <Variable value="ctall_medium_adhesion_init" symbol="ctall_medium_adhesion"/>
        <Variable value="0" symbol="ct1_perturbation_time" name="time_of_knockout_initiation_ct1_adhesion"/>
        <Constant value="-100" symbol="adhesion_init_ct1"/>
        <Variable value="-100" symbol="ct1_adhesion_weak"/>
        <Variable value="1.12" symbol="ct1_membraneElasticity_init"/>
        <Variable value="1.12" symbol="ct1_membraneElasticity"/>
        <Variable value="1.12" symbol="ct1_membraneElasticity_final"/>
        <Variable value="1.32" symbol="ct1_membraneElasticity_edge_init"/>
        <Variable value="1.32" symbol="ct1_membraneElasticity_edge"/>
        <Variable value="1.32" symbol="ct1_membraneElasticity_final_edge"/>
        <Variable value="48.28" symbol="ct1_k_half_hours"/>
        <Variable value="5.0" symbol="ct1_hill_n" name=""/>
        <Variable value="137" symbol="ct1_area_init"/>
        <Variable value="137" symbol="ct1_area" name=""/>
        <Variable value="137" symbol="ct1_area_final" name=""/>
        <Variable value="34.03" symbol="ct1_area_final_sd"/>
        <Variable value="177" symbol="ct1_area_edge_init"/>
        <Variable value="177" symbol="ct1_area_edge" name=""/>
        <Variable value="177" symbol="ct1_area_edge_final" name=""/>
        <Variable value="49.66" symbol="ct1_area_edge_final_sd"/>
        <Variable value="20" symbol="ct1_generation_time_init"/>
        <Variable value="20" symbol="ct1_generation_time_final"/>
        <Variable value="20" symbol="ct1_generation_time"/>
        <Variable value="0.5" symbol="ct1_membrane_str_init"/>
        <Variable value="0.5" symbol="ct1_membrane_str_final"/>
        <Variable value="0.5" symbol="ct1_membrane_str"/>
        <Variable value="9" symbol="ct1_persistent_motion_str_init"/>
        <Variable value="9" symbol="ct1_persistent_motion_str_final"/>
        <Variable value="9" symbol="ct1_persistent_motion"/>
        <Variable value="0" symbol="ct2_perturbation_time" name="time_of_knockout_initiation_ct2_adhesion"/>
        <Constant value="-100" symbol="adhesion_init_ct2"/>
        <Variable value="-100" symbol="ct2_adhesion_weak"/>
        <Variable value="1.12" symbol="ct2_membraneElasticity_init"/>
        <Variable value="1.12" symbol="ct2_membraneElasticity"/>
        <Variable value="1.12" symbol="ct2_membraneElasticity_final"/>
        <Variable value="1.32" symbol="ct2_membraneElasticity_edge_init"/>
        <Variable value="1.32" symbol="ct2_membraneElasticity_edge"/>
        <Variable value="1.32" symbol="ct2_membraneElasticity_final_edge"/>
        <Variable value="48.28" symbol="ct2_k_half_hours"/>
        <Variable value="5.0" symbol="ct2_hill_n"/>
        <Variable value="137" symbol="ct2_area_init"/>
        <Variable value="137" symbol="ct2_area"/>
        <Variable value="34.03" symbol="ct2_area_final_sd"/>
        <Variable value="137" symbol="ct2_area_final"/>
        <Variable value="177" symbol="ct2_area_edge_init"/>
        <Variable value="177" symbol="ct2_area_edge"/>
        <Variable value="177" symbol="ct2_area_edge_final"/>
        <Variable value="49.66" symbol="ct2_area_edge_final_sd"/>
        <Variable value="20" symbol="ct2_generation_time_init"/>
        <Variable value="20" symbol="ct2_generation_time_final"/>
        <Variable value="20" symbol="ct2_generation_time"/>
        <Variable value="0.5" symbol="ct2_membrane_str_init"/>
        <Variable value="0.5" symbol="ct2_membrane_str_final"/>
        <Variable value="0.5" symbol="ct2_membrane_str"/>
        <Variable value="9" symbol="ct2_persistent_motion_str_init"/>
        <Variable value="9" symbol="ct2_persistent_motion_str_final"/>
        <Variable value="9" symbol="ct2_persistent_motion"/>
        <System time-step="0.1" solver="Euler [fixed, O(1)]" name="ct1_mechanics_updater">
            <Rule symbol-ref="ct1_adhesion">
                <Expression>if(time &lt; burn_in_time, adhesion_init_ct1,
  if(time > ct1_perturbation_time
  and ct1_adhesion >= adhesion_init_ct1
  and ct1_adhesion &lt;= ct1_adhesion_weak,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))* (adhesion_init_ct1-ct1_adhesion_weak) + ct1_adhesion_weak,
  if(time &lt;= ct1_perturbation_time,adhesion_init_ct1,ct1_adhesion_weak)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct1_membraneElasticity">
                <Expression>if(time &lt; burn_in_time,ct1_membraneElasticity_init,
  if(time > ct1_perturbation_time 
  and ct1_membraneElasticity >= ct1_membraneElasticity_init
  and ct1_membraneElasticity &lt;= ct1_membraneElasticity_final,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))*(ct1_membraneElasticity_init-ct1_membraneElasticity_final) + ct1_membraneElasticity_final,
  if(time &lt;= ct1_perturbation_time,ct1_membraneElasticity_init,ct1_membraneElasticity_final)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct1_membraneElasticity_edge">
                <Expression>if(time &lt; burn_in_time,ct1_membraneElasticity_edge_init,
  if(time > ct1_perturbation_time 
    and ct1_membraneElasticity_edge >= ct1_membraneElasticity_edge_init
    and ct1_membraneElasticity_edge &lt;= ct1_membraneElasticity_final_edge,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))*(ct1_membraneElasticity_edge_init-ct1_membraneElasticity_final_edge) + ct1_membraneElasticity_final_edge,
  if(time &lt;= ct1_perturbation_time,ct1_membraneElasticity_edge_init,ct1_membraneElasticity_final_edge)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct1_area">
                <Expression>if(time >burn_in_time,ct1_area_init,
 if( (ct1_area_init &lt; ct1_area_final) 
     and ct1_area &lt; ct1_area_final,
ct1_area_init + 
(ct1_area_final-ct1_area_init)*
((time-ct1_perturbation_time)/96),
ct1_area_final)
)</Expression>
            </Rule>
            <Rule symbol-ref="ct1_area_edge">
                <Expression>if(time &lt; burn_in_time,ct1_area_edge_init,
  if((ct1_area_edge_init &lt; ct1_area_edge_final) 
     and ct1_area_edge &lt; ct1_area_edge_final,
ct1_area_edge_init + 
(ct1_area_edge_final-ct1_area_edge_init)*
((time-ct1_perturbation_time)/96),
ct1_area_edge_final)
)
                </Expression>
            </Rule>
            <Rule symbol-ref="ct1_generation_time">
                <Expression>if(time &lt; burn_in_time, ct1_generation_time_init,
  if(time > ct1_perturbation_time,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))* (ct1_generation_time_init-ct1_generation_time_final) + ct1_generation_time_final,
  if(time &lt;= ct1_perturbation_time,ct1_generation_time_init,ct1_generation_time_final)
))
</Expression>
            </Rule>
            <Rule symbol-ref="ct1_membrane_str">
                <Expression>if(time &lt; burn_in_time, ct1_membrane_str_init,
  if(time > ct1_perturbation_time
  and ct1_membrane_str >= ct1_membrane_str_init
  and ct1_membrane_str &lt;= ct1_membrane_str_final,
    (1.0 / (1.0 + (pow(((time-ct1_perturbation_time)/ct1_k_half_hours),ct1_hill_n))))* (ct1_membrane_str_init-ct1_membrane_str_final) + ct1_membrane_str_final,
  if(time &lt;= ct1_perturbation_time,ct1_membrane_str_init,ct1_membrane_str_final)
))</Expression>
            </Rule>
        </System>
        <System time-step="0.1" solver="Euler [fixed, O(1)]" name="ct2_mechanics_updater">
            <Rule symbol-ref="ct2_adhesion">
                <Expression>if(time &lt; burn_in_time,adhesion_init_ct2,
  if(time > ct2_perturbation_time 
  and ct2_adhesion >= adhesion_init_ct2
  and ct2_adhesion &lt;= ct2_adhesion_weak,
  (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))*(adhesion_init_ct2-ct2_adhesion_weak) + ct2_adhesion_weak,
  if(time &lt;= ct2_perturbation_time,adhesion_init_ct2,ct2_adhesion_weak)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct2_membraneElasticity">
                <Expression>if(time &lt; burn_in_time,ct2_membraneElasticity_init,
  if(time > ct2_perturbation_time 
  and ct2_membraneElasticity >= ct2_membraneElasticity_init
  and ct2_membraneElasticity &lt;= ct2_membraneElasticity_final,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))*
(ct2_membraneElasticity_init-ct2_membraneElasticity_final) + ct2_membraneElasticity_final,
  if(time &lt;= ct2_perturbation_time,ct2_membraneElasticity_init,ct2_membraneElasticity_final)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct2_membraneElasticity_edge">
                <Expression>if(time &lt; burn_in_time,ct2_membraneElasticity_edge_init,
  if(time > ct2_perturbation_time 
  and ct2_membraneElasticity_edge >= ct2_membraneElasticity_edge_init
  and ct2_membraneElasticity_edge &lt;= ct2_membraneElasticity_final_edge,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))*(ct2_membraneElasticity_edge_init-ct2_membraneElasticity_final_edge) + ct2_membraneElasticity_final_edge,
  if(time &lt;= ct2_perturbation_time,ct2_membraneElasticity_edge_init,ct2_membraneElasticity_final_edge)
))</Expression>
            </Rule>
            <Rule symbol-ref="ct2_area">
                <Expression>if(time &lt; burn_in_time,ct2_area_init,
  if( (ct2_area_init &lt; ct2_area_final)
    and ct2_area &lt; ct2_area_final,
    ct2_area_init + (ct2_area_final-ct2_area_init)*
    ((time-ct2_perturbation_time)/96),
  ct2_area_final)
)             </Expression>
            </Rule>
            <Rule symbol-ref="ct2_area_edge">
                <Expression>if(time &lt; burn_in_time,ct2_area_edge_init,
  if( (ct2_area_edge_init &lt; ct2_area_edge_final)
     and ct2_area_edge &lt; ct2_area_edge_final,
    ct2_area_edge_init + 
    (ct2_area_edge_final-ct2_area_edge_init)*
    ((time-ct2_perturbation_time)/96),
  ct2_area_edge_final)
)</Expression>
            </Rule>
            <Rule symbol-ref="ct2_generation_time">
                <Expression>if(time &lt; burn_in_time, ct2_generation_time_init,
  if(time > ct2_perturbation_time,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct2_k_half_hours),ct2_hill_n))))* (ct2_generation_time_init-ct2_generation_time_final) + ct2_generation_time_final,
  if(time &lt;= ct2_perturbation_time,ct2_generation_time_init,ct2_generation_time_final)
))
</Expression>
            </Rule>
            <Rule symbol-ref="ct2_membrane_str">
                <Expression>if(time &lt; burn_in_time, ct2_membrane_str_init,
  if(time > ct2_perturbation_time
  and ct2_membrane_str >= ct2_membrane_str_init
  and ct2_membrane_str &lt;= ct2_membrane_str_final,
    (1.0 / (1.0 + (pow(((time-ct2_perturbation_time)/ct1_k_half_hours),ct2_hill_n))))* (ct2_membrane_str_init-ct2_membrane_str_final) + ct2_membrane_str_final,
  if(time &lt;= ct2_perturbation_time,ct2_membrane_str_init,ct2_membrane_str_final)
))</Expression>
            </Rule>
        </System>
        <System time-step="0.1" solver="Euler [fixed, O(1)]" name="adhesion_ct1_ct2">
            <Rule symbol-ref="ct1_ct2">
                <Expression>max(ct1_adhesion,ct2_adhesion)</Expression>
            </Rule>
        </System>
        <Constant value="0" symbol="wt_offset_x"/>
        <Constant value="0" symbol="wt_offset_y"/>
        <Constant value="0" symbol="inhibited_offset_x"/>
        <Constant value="0" symbol="inhibited_offset_y"/>
        <Field value="0" symbol="U" name="chemoattractant">
            <Diffusion rate="0"/>
        </Field>
        <!--    <Disabled>
        <System time-step="1" solver="Euler [fixed, O(1)]">
            <DiffEqn symbol-ref="U">
                <Expression>p-degp*U</Expression>
            </DiffEqn>
        </System>
    </Disabled>
-->
        <!--    <Disabled>
        <Constant value="0.003" symbol="cell_density"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Field value="0" symbol="protrusionMemory">
            <Diffusion rate="0.0"/>
        </Field>
    </Disabled>
-->
        <Variable value="50" symbol="num_cells_ct2"/>
        <Variable value="100-num_cells_ct2" symbol="num_cells_ct1"/>
        <Variable value="1" symbol="avgMediaNeighborLength" name="percent_membrane_bordeing_media"/>
        <Variable value="5" symbol="burn_in_time"/>
    </Global>
    <Space>
        <Lattice class="square">
            <Size value="1200,1200,0" symbol="lattice"/>
            <BoundaryConditions>
                <Condition boundary="x" type="constant"/>
                <Condition boundary="y" type="constant"/>
            </BoundaryConditions>
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="120" symbol="hours"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="43263"/>
    </Time>
    <CellTypes>
        <CellType name="ct1" class="biological">
            <Property value="0" symbol="d" name="divisions"/>
            <Property value="0" symbol="c" name="color"/>
            <Property value="ct1_generation_time" symbol="dt" name="generationTime"/>
            <Property value="0.0" symbol="dc" name="division_time_counter"/>
            <Property value="1.0" symbol="cell" name="cell (for counting)"/>
            <Property value="ct1_adhesion" symbol="cadherin"/>
            <Property value="ct1_ct2" symbol="ct1_ct2_adhesion"/>
            <!--    <Disabled>
        <Property value="0.5" symbol="percentDaughterVolume" name="percentVolume2DaughterCell"/>
    </Disabled>
-->
            <!--    <Disabled>
        <Constant value="32.0" symbol="maxTargetArea"/>
    </Disabled>
-->
            <!--    <Disabled>
        <Property value="0.5*maxTargetArea/dt" symbol="cellVolumeIncrement"/>
    </Disabled>
-->
            <Property value="ct1_area" symbol="targetArea"/>
            <Property value="ct1_membraneElasticity" symbol="targetSurface"/>
            <VolumeConstraint strength="1.0" target="targetArea"/>
            <SurfaceConstraint mode="aspherity" strength="ct1_membrane_str" target="targetSurface"/>
            <PropertyVector value="0.0, 0.0, 0.0" symbol="dv" name="direction_vector"/>
            <PersistentMotion decay-time="0.003" protrusion="true" strength="9"/>
            <!--    <Disabled>
        <Event trigger="when true" name="partitionCell">
            <Condition>dc == (dt-1)</Condition>
            <Rule symbol-ref="percentDaughterVolume">
                <Expression>rand_uni(0.45,0.55)</Expression>
            </Rule>
        </Event>
    </Disabled>
-->
            <CellDivision division-plane="minor" daughterID="daughter">
                <Condition>dc >= dt</Condition>
                <Triggers name="">
                    <Rule symbol-ref="dc">
                        <Expression>0</Expression>
                    </Rule>
                    <!--    <Disabled>
        <Rule symbol-ref="targetArea">
            <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
        </Rule>
    </Disabled>
-->
                </Triggers>
            </CellDivision>
            <System time-step="1" solver="Euler [fixed, O(1)]" name="cell_state_updater">
                <Rule symbol-ref="dc">
                    <Expression>dc+1</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="c">
            <Expression>if(isEdgeCell > 0,2,0)</Expression>
        </Rule>
    </Disabled>
-->
                <!--    <Disabled>
        <Rule symbol-ref="targetArea">
            <Expression>if((time-ct1_perturbation_time) > ct1_k_half_hours,
  if(isEdgeCell > 0,
    cell_area_edge_final, 
    cell_area_final),
  if(isEdgeCell > 0,
    cell_area_edge,
    cell_area)  
)</Expression>
        </Rule>
    </Disabled>
-->
                <Rule symbol-ref="targetSurface">
                    <Expression>if(isEdgeCell > 0,
ct1_membraneElasticity_edge,
ct1_membraneElasticity)</Expression>
                </Rule>
            </System>
            <!--    <Disabled>
        <PropertyVector value="0.0, 0.0, 0.0" symbol="displacement"/>
    </Disabled>
-->
            <!--    <Disabled>
        <MotilityReporter time-step="1">
            <Displacement symbol-ref="displacement"/>
        </MotilityReporter>
    </Disabled>
-->
            <Property value="10" symbol="p"/>
            <Chemotaxis field="U" retraction="true" strength="3" contact-inhibition="true"/>
            <Constant value="0.9" symbol="degp"/>
            <!--    <Disabled>
        <Protrusion maximum="10" field="protrusionMemory" strength="1"/>
    </Disabled>
-->
            <Property value="0" symbol="isEdgeCell"/>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.medium.id"/>
                <Output symbol-ref="isEdgeCell" mapping="sum"/>
            </NeighborhoodReporter>
            <Property value="ctall_medium_adhesion_init" symbol="ctall_medium_adhesion"/>
            <Property value="0" symbol="cell_area_final"/>
            <Property value="0" symbol="cell_area_edge_final"/>
        </CellType>
        <CellType name="ct2" class="biological">
            <Property value="0" symbol="d" name="divisions"/>
            <Property value="1" symbol="c" name="color"/>
            <Property value="1.0" symbol="cell" name="cell (for counting)"/>
            <Property value="ct2_generation_time" symbol="dt" name="generationTime"/>
            <Property value="0.0" symbol="dc" name="division_time_counter"/>
            <Property value="ct2_adhesion" symbol="cadherin" name="global_cadherin_inhibitedpop"/>
            <Property value="ct1_ct2" symbol="ct1_ct2_adhesion"/>
            <Property value="ctall_medium_adhesion_init" symbol="ctall_medium_adhesion"/>
            <Property value="0" symbol="avgLikeNeighborCount"/>
            <Property value="0" symbol="avgLikeNeighborLength"/>
            <Property value="ct2_area" symbol="targetArea"/>
            <Property value="ct2_membraneElasticity" symbol="targetSurface"/>
            <VolumeConstraint strength="1" target="targetArea"/>
            <SurfaceConstraint mode="aspherity" strength="ct2_membrane_str" target="targetSurface"/>
            <PropertyVector value="0.0, 0.0, 0.0" symbol="dv" name="direction_vector"/>
            <PersistentMotion decay-time="0.003" protrusion="true" strength="9"/>
            <CellDivision division-plane="major" daughterID="daughter">
                <Condition>dc >= dt</Condition>
                <Triggers name="">
                    <Rule symbol-ref="dc">
                        <Expression>0</Expression>
                    </Rule>
                    <!--    <Disabled>
        <Rule symbol-ref="targetArea">
            <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
        </Rule>
    </Disabled>
-->
                </Triggers>
            </CellDivision>
            <System time-step="1" solver="Euler [fixed, O(1)]" name="cell_state_updater">
                <Rule symbol-ref="dc">
                    <Expression>dc+1</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="c">
            <Expression>if(isEdgeCell > 0,2,1)</Expression>
        </Rule>
    </Disabled>
-->
                <Rule symbol-ref="targetArea" name="CellGrowth">
                    <Expression>if(isEdgeCell > 0,ct2_area_edge,ct2_area)</Expression>
                </Rule>
                <Rule symbol-ref="targetSurface">
                    <Expression>if(isEdgeCell > 0,ct2_membraneElasticity_edge,ct2_membraneElasticity)</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="ctall_medium_adhesion" name="peripheral_cell_migration">
            <Expression>if(avgMediaNeighborLength > 0.35,max(cadherin,ct1_ct2_adhesion)/2,0)</Expression>
        </Rule>
    </Disabled>
-->
            </System>
            <!--    <Disabled>
        <PropertyVector value="0.0, 0.0, 0.0" symbol="displacement"/>
    </Disabled>
-->
            <!--    <Disabled>
        <MotilityReporter time-step="1">
            <Displacement symbol-ref="displacement"/>
        </MotilityReporter>
    </Disabled>
-->
            <Property value="10" symbol="p"/>
            <Constant value="0.9" symbol="degp"/>
            <Chemotaxis field="U" retraction="true" strength="3" contact-inhibition="true"/>
            <!--    <Disabled>
        <Protrusion maximum="10" field="protrusionMemory" strength="1"/>
    </Disabled>
-->
            <Property value="0" symbol="isEdgeCell"/>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.medium.id"/>
                <Output symbol-ref="isEdgeCell" mapping="sum"/>
            </NeighborhoodReporter>
            <Property value="0" symbol="cell_area_final"/>
            <Property value="0" symbol="cell_area_edge_final"/>
        </CellType>
        <!--    <Disabled>
        <CellType name="collagen" class="biological">
            <Property value="0" symbol="d" name="divisions"/>
            <Property value="2" symbol="c" name="color"/>
            <Property value="1.0" symbol="cell" name="cell (for counting)"/>
            <Disabled>
                <Property value="20" symbol="dt" name="generationTime"/>
            </Disabled>
            <Property value="0.0" symbol="dc" name="division_time_counter"/>
            <Property value="10" symbol="targetArea"/>
            <Property value="2" symbol="cadherin" name="global_cadherin_inhibitedpop"/>
            <Disabled>
                <Property value="0" symbol="avgLikeNeighborCount"/>
            </Disabled>
            <Disabled>
                <Property value="0" symbol="avgLikeNeighborLength"/>
            </Disabled>
            <VolumeConstraint strength="1" target="targetArea"/>
            <PropertyVector value="0.0, 0.0, 0.0" symbol="dv" name="direction_vector"/>
            <Disabled>
                <CellDivision division-plane="major" daughterID="daughter">
                    <Condition>dc >= dt</Condition>
                    <Triggers name="">
                        <Rule symbol-ref="dc">
                            <Expression>0</Expression>
                        </Rule>
                        <Disabled>
                            <Rule symbol-ref="targetArea">
                                <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
                            </Rule>
                        </Disabled>
                    </Triggers>
                </CellDivision>
            </Disabled>
            <Property value="2" symbol="ct1_ct2_adhesion"/>
            <Property value="0.10" symbol="p"/>
            <Disabled>
                <Chemotaxis field="U" retraction="true" strength="c" contact-inhibition="false"/>
            </Disabled>
            <FreezeMotion>
                <Condition>time >=5</Condition>
            </FreezeMotion>
        </CellType>
    </Disabled>
-->
        <CellType name="medium" class="medium">
            <Property value="ctall_medium_adhesion" symbol="cadherin"/>
            <Property value="0" symbol="avgLikeNeighborCount"/>
            <Property value="0" symbol="avgLikeNeighborLength"/>
            <Property value="0" symbol="p"/>
            <!--    <Disabled>
        <Property value="0" symbol="dc"/>
    </Disabled>
-->
            <Constant value="0.0" symbol="degp"/>
        </CellType>
        <!--    <Disabled>
        <CellType name="sisterColony" class="biological">
            <Property value="0" symbol="d" name="divisions"/>
            <Property value="3" symbol="c" name="color"/>
            <Property value="1.0" symbol="cell" name="cell (for counting)"/>
            <Disabled>
                <Property value="20" symbol="dt" name="generationTime"/>
            </Disabled>
            <Property value="0.0" symbol="dc" name="division_time_counter"/>
            <Property value="500" symbol="targetArea"/>
            <Property value="2" symbol="cadherin" name="global_cadherin_inhibitedpop"/>
            <Disabled>
                <Property value="0" symbol="avgLikeNeighborCount"/>
            </Disabled>
            <Disabled>
                <Property value="0" symbol="avgLikeNeighborLength"/>
            </Disabled>
            <VolumeConstraint strength="1" target="targetArea"/>
            <PropertyVector value="0.0, 0.0, 0.0" symbol="dv" name="direction_vector"/>
            <Disabled>
                <CellDivision division-plane="major" daughterID="daughter">
                    <Condition>dc >= dt</Condition>
                    <Triggers name="">
                        <Rule symbol-ref="dc">
                            <Expression>0</Expression>
                        </Rule>
                        <Disabled>
                            <Rule symbol-ref="targetArea">
                                <Expression>if( daughter == 1, 
percentDaughterVolume*maxTargetArea, 
(1-percentDaughterVolume)*maxTargetArea )</Expression>
                            </Rule>
                        </Disabled>
                    </Triggers>
                </CellDivision>
            </Disabled>
            <Property value="2" symbol="ct1_ct2_adhesion"/>
            <Property value="500" symbol="p"/>
            <Disabled>
                <Chemotaxis field="U" retraction="true" strength="c" contact-inhibition="false"/>
            </Disabled>
            <Disabled>
                <FreezeMotion>
                    <Condition>time >=5</Condition>
                </FreezeMotion>
            </Disabled>
            <SurfaceConstraint mode="aspherity" strength="1" target="0.5"/>
        </CellType>
    </Disabled>
-->
    </CellTypes>
    <CPM>
        <Interaction default="0">
            <Contact value="0" type1="ct1" type2="medium">
                <HomophilicAdhesion adhesive="ctall_medium_adhesion" strength="1"/>
            </Contact>
            <Contact value="0" type1="ct2" type2="medium">
                <HomophilicAdhesion adhesive="ctall_medium_adhesion" strength="1"/>
            </Contact>
            <Contact value="0" type1="ct1" type2="ct1">
                <HomophilicAdhesion adhesive="cadherin" strength="1"/>
            </Contact>
            <Contact value="0" type1="ct1" type2="ct2">
                <HomophilicAdhesion adhesive="ct1_ct2" strength="1"/>
            </Contact>
            <Contact value="0" type1="ct2" type2="ct2">
                <HomophilicAdhesion adhesive="cadherin" strength="1"/>
            </Contact>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="0.03"/>
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
            <MetropolisKinetics yield="0.1" temperature="10"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population size="1" type="ct1" name="cell_type_1">
            <!--    <Disabled>
        <InitCircle mode="random" number-of-cells="75" random_displacement="0.3*lattice.x">
            <Dimensions center="lattice.x/2,lattice.y/2,0" radius="20"/>
        </InitCircle>
    </Disabled>
-->
            <InitProperty symbol-ref="dc">
                <Expression>rand_uni(0,dt)</Expression>
            </InitProperty>
            <InitRectangle mode="regular" random-offset="10" number-of-cells="num_cells_ct1">
                <Dimensions size="60,20,0" origin="(lattice.x/2)+wt_offset_x,(lattice.y/2)+wt_offset_y,0"/>
            </InitRectangle>
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_final">
            <Expression>rand_norm(ct1_area_final,ct1_area_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_edge_final">
            <Expression>rand_norm(ct1_area_edge_final
    ,ct1_area_edge_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
        </Population>
        <Population size="1" type="ct2" name="cell_type_2">
            <InitRectangle mode="regular" random-offset="10" number-of-cells="num_cells_ct2">
                <Dimensions size="60,20,0" origin="(lattice.x/2)+inhibited_offset_x,(lattice.y/2)+inhibited_offset_y,0"/>
            </InitRectangle>
            <!--    <Disabled>
        <InitCircle mode="random" number-of-cells="25" random_displacement="0.05*lattice.x">
            <Dimensions center="lattice.x/2, lattice.y/2,0" radius="10"/>
        </InitCircle>
    </Disabled>
-->
            <InitProperty symbol-ref="dc">
                <Expression>rand_uni(0,dt)</Expression>
            </InitProperty>
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_final">
            <Expression>rand_norm(ct2_area_final,ct2_area_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
            <!--    <Disabled>
        <InitProperty symbol-ref="cell_area_edge_final">
            <Expression>rand_norm(ct2_area_edge_final
    ,ct2_area_edge_final_sd)</Expression>
        </InitProperty>
    </Disabled>
-->
        </Population>
        <!--    <Disabled>
        <Population size="1" type="collagen">
            <InitRectangle mode="regular" number-of-cells="cell_density*lattice.*lattice.y">
                <Dimensions size="lattice.x/2,lattice.y,0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </Disabled>
-->
        <!--    <Disabled>
        <Population size="1" type="sisterColony">
            <InitCircle mode="regular" number-of-cells="1">
                <Dimensions center="100,100,0" radius="1"/>
            </InitCircle>
        </Population>
    </Disabled>
-->
        <!--    <Disabled>
        <Population size="1" type="ct1" name="wildtype">
            <InitCircle mode="random" number-of-cells="75" random_displacement="0.3*lattice.x">
                <Dimensions center="50,50,0" radius="20"/>
            </InitCircle>
            <InitProperty symbol-ref="dc">
                <Expression>rand_uni(0,dt)</Expression>
            </InitProperty>
            <Disabled>
                <InitProperty symbol-ref="targetArea">
                    <Expression>rand_uni(16,32)</Expression>
                </InitProperty>
            </Disabled>
        </Population>
    </Disabled>
-->
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="1" decorate="false">
            <Terminal size="1200,1200,0" name="png"/>
            <Plot>
                <Cells value="c" max="3" min="0">
                    <ColorMap>
                        <Color value="0" color="gray"/>
                        <Color value="1" color="red"/>
                        <Color value="2" color="yellow"/>
                        <Color value="3" color="blue"/>
                    </ColorMap>
                </Cells>
                <!--    <Disabled>
        <Field symbol-ref="U" surface="true" max="100" min="0.0"/>
    </Disabled>
-->
            </Plot>
        </Gnuplotter>
        <!--    <Disabled>
        <HistogramLogger normalized="true" maximum="32" time-step="1" minimum="14" number-of-bins="20">
            <Column symbol-ref="targetArea" celltype="ct2"/>
            <Plot terminal="png" maximum="1" minimum="0.0"/>
        </HistogramLogger>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="1">
            <Input>
                <Disabled>
                    <Symbol symbol-ref="cell.id"/>
                </Disabled>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="cell.type"/>
                <Disabled>
                    <Symbol symbol-ref="avgMediaNeighborLength"/>
                </Disabled>
                <Disabled>
                    <Symbol symbol-ref="avgLikeNeighborLength"/>
                </Disabled>
            </Input>
            <Output>
                <TextOutput file-numbering="time" file-name="cell_positions"/>
            </Output>
        </Logger>
    </Disabled>
-->
        <Logger time-step="1">
            <Input>
                <Symbol symbol-ref="ct1_adhesion"/>
                <Symbol symbol-ref="ct1_membrane_str"/>
                <Symbol symbol-ref="ct1_area"/>
                <Symbol symbol-ref="ct1_area_edge"/>
                <Symbol symbol-ref="ct1_membraneElasticity"/>
                <Symbol symbol-ref="ct1_membraneElasticity_edge"/>
                <Symbol symbol-ref="ct2_adhesion"/>
                <Symbol symbol-ref="ct2_membrane_str"/>
                <Symbol symbol-ref="ct2_area"/>
                <Symbol symbol-ref="ct2_area_edge"/>
                <Symbol symbol-ref="ct2_membraneElasticity"/>
                <Symbol symbol-ref="ct2_membraneElasticity_edge"/>
            </Input>
            <Output>
                <TextOutput file-numbering="time" file-name="cell_membrane_properties"/>
            </Output>
            <!--    <Disabled>
        <Plots>
            <Plot time-step="1">
                <Style style="linespoints" grid="true"/>
                <Terminal terminal="png"/>
                <X-axis>
                    <Symbol symbol-ref="time"/>
                </X-axis>
                <Y-axis>
                    <Symbol symbol-ref="ct1_adhesion"/>
                    <Symbol symbol-ref="ct2_adhesion"/>
                </Y-axis>
            </Plot>
        </Plots>
    </Disabled>
-->
        </Logger>
        <!--    <Disabled>
        <DisplacementTracker celltype="ct1" time-step="1"/>
    </Disabled>
-->
        <!--    <Disabled>
        <DisplacementTracker celltype="ct2" time-step="1"/>
    </Disabled>
-->
        <ModelGraph include-tags="#untagged" format="dot" reduced="true"/>
    </Analysis>
</MorpheusModel>
