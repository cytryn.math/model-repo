---
MorpheusModelID: M6694

authors: [M. R. Regueira, J. D. García, A. R.-P. Aradas]
contributors: [M. R. Regueira]

title: "Diffusion model of gap genes in Drosophila melanogaster"
date: "2019-03-16T00:00:00+01:00"
hidden: true
#publishDate: ...
lastmod: "2021-09-23T14:32:00+02:00"

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume: 
#  issue: 
#  page: ""
#  year:
#  original_model: true

#tags:
#- A-P Patterning
#- Anterior-Posterior Patterning
#- Bicoid
#- bcd
#- Blastoderm
#- Blastoderm Stage
#- cad
#- Caudal
#- Cellular Potts Model
#- CPM
#- Drosophila
#- Drosophila melanogaster
#- Embryogenesis
#- Embryo Segmentation
#- eve
#- even-skipped
#- Feed-Forward Loop
#- FFL
#- IFFL
#- Gap Gene
#- Giant
#- gt
#- hb
#- Hunchback
#- Incoherent Feed-Forward Loop
#- kni
#- Knirps
#- kr
#- Krüppel
#- Maternal Effect Gene
#- Morphogen
#- Morphogenesis
#- Multicellular Incoherent Feed-Forward Loop
#- Multiscale Model
#- mIFFL
#- Nanos
#- nos
#- ODE
#- Ordinary Differential Equation
#- Pair-Rule Gene
#- Partial Differential Equation
#- Pattern Formation
#- PDE
#- Segmentation
#- Spatial Pattern
#- Stripe Pattern
#- Tailless
#- Terminal Gene
#- tll

#categories:
#- DOI:...

# order
# weight: ...
---
## Reference

This model is used in the publication by Regueira *et al.* under review.