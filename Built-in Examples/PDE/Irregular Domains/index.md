---
MorpheusModelID: M0013

authors: [A. Gierer, H. Meinhardt]

title: "Domains: Reaction-Diffusion in Irregular Domains"
date: "2019-11-05T15:29:00+01:00"
lastmod: "2020-10-30T12:32:00+01:00"

aliases: [/examples/domains-reaction-diffusion-in-irregular-domains/]

menu:
  Built-in Examples:
    parent: PDE
    weight: 30
weight: 80
---

## Introduction

A 2D activator-inhibitor model ([Gierer and Meinhardt, 1972][gierer-1972]), solved in a irregular domain that is loaded from an [image file][domain].

![](domains.png "Spot pattern in Gierer-Meinhardt model with irregular domain.")

## Description

This model uses an irregular ```Domain``` with ```constant``` boundary conditions. The domain is loaded from a [TIFF image][domain]. See ```Space/Lattice/Domain```.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/70395104?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Reference

A. Gierer, H. Meinhardt: [A Theory of Biological Pattern Formation.][gierer-1972] *Kybernetik* **12**: 30-39, 1972.

[gierer-1972]: http://www.eb.tuebingen.mpg.de/fileadmin/uploads/pdf/Emeriti/Hans_Meinhardt/kyb.pdf
[domain]: domain.tiff